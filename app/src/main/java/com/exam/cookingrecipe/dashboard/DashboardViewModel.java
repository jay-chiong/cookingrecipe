package com.exam.cookingrecipe.dashboard;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.repository.RecipeRepository;
import com.exam.cookingrecipe.utils.ImageConverter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class DashboardViewModel extends AndroidViewModel
{
    private RecipeRepository mRepository;

    public final MutableLiveData<List<Recipe>> mRecipes = new MutableLiveData<>();

    public final ObservableField<Recipe> selectedRecipe = new ObservableField<>();

    public DashboardViewModel(@NonNull Application application)
    {
        super(application);

        this.mRepository = new RecipeRepository(application);
    }

    public LiveData<List<Recipe>> getRecipes()
    {
        return this.mRepository.getAllRecipes();
    }

    public LiveData<List<Recipe>> getFilteredRecipes(String recipeType)
    {
        return this.mRepository.getFilteredRecipes(recipeType);
    }

    public void populateRecipes(FragmentActivity activity)
    {
        ArrayList<Recipe> recipes = new ArrayList<>();

        recipes.add(new Recipe(
                "Banana Oatmeal Muffins",
                "Breads, Muffin and Scones",
                "1 cup quick rolled oats\n1/4 cup milk\n2 eggs, lightly beaten\n1/3 cup oil\n" +
                        "1 cup mashed ripe bananas\n1 1/2 cups whole wheat flour\n1/2 cup sugar\n" +
                        "2 teaspoons baking soda\n1/4 teaspoon salt",
                "1. Preheat oven to 400 degrees F. Lightly oil or spray the bottom of 12 muffin cups.\n" +
                        "2. Mix oats with milk. Stir in lightly-beaten eggs, oil and bananas. " +
                        "Let stand while measuring dry ingredients\n" +
                        "3. In a separate bow., combine dry ingredients and stir well.\n" +
                        "4. Add oat mixture to dry ingredients and stir gently to moisten. Don't overmix.\n" +
                        "5. Fill muffin cups 3/4 full.\n" +
                        "6. Bake at 400 degrees F until golden brown and a toothpick inserted in the " +
                        "center comes out moist but clean, about 18 to 20 minutes.",
                        ImageConverter.bitmapToByteArray(BitmapFactory.decodeResource(activity.getResources(),
                                R.drawable.banana_oatmeal_muffins)))
                );

        recipes.add(new Recipe(
                "Almond Rice Pudding",
                "Breakfast",
                "3 cups almond milk\n1 cup white or brown rice, uncooked\n1/4 cup sugar\n" +
                        "1 teaspoon vanilla\ncinnamon to taste\n1/4 cup toasted almonds (optional)",
                "1. Combine almond milk and rice in a 2-3 quart saucepan, and bring to a boil.\n" +
                        "2. Reduce heal and simmer for 1/2 hour with lid on until the rice is soft.\n" +
                        "3. Add sugar, vanilla, almond extract and cinnamon. Stir and serve warm.\n" +
                        "4. Refrigerate leftovers within 2 hours.",
                        ImageConverter.bitmapToByteArray(BitmapFactory.decodeResource(activity.getResources(),
                            R.drawable.almond_rice_pudding)))
                );

        this.mRepository.insertAll(recipes);
    }

    public void setSelectedRecipe(Recipe recipe)
    {
        this.selectedRecipe.set(recipe);
    }
}
