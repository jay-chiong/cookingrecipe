package com.exam.cookingrecipe.dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.dashboard.adapter.RecipeListAdapter;
import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.filter.FilterFragment;
import com.exam.cookingrecipe.filter.FilterViewModel;

import java.util.List;
import java.util.logging.Filter;

public class DashboardActivity extends AppCompatActivity
{
    private DashboardViewModel mDashboardViewModel;
    private FilterViewModel mFilterViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);

        setupViewModel();
        setupActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.dashboard_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_filter:
                FilterFragment filterFragment = new FilterFragment();
                filterFragment.show(getSupportFragmentManager(), "dialog");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.mDashboardViewModel.selectedRecipe.set(null);
    }

    private void setupViewModel()
    {
        this.mDashboardViewModel = this.obtainDashboardViewModel(this);
        this.mFilterViewModel = this.obtainFilterViewModel(this);
    }

    private void setupActivity()
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new DashboardFragment());
        transaction.commit();
    }

    private DashboardViewModel obtainDashboardViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(DashboardViewModel.class);
    }

    private FilterViewModel obtainFilterViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(FilterViewModel.class);
    }
}
