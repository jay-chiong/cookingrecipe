package com.exam.cookingrecipe.dashboard.adapter;

import android.content.Context;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.dashboard.DashboardViewModel;
import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.databinding.RecipeListViewBinding;
import com.exam.cookingrecipe.helper.RecipeUserActionListener;

import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder>
{

    class RecipeViewHolder extends RecyclerView.ViewHolder
    {
        final RecipeListViewBinding binding;

        private RecipeViewHolder(RecipeListViewBinding itemBinding)
        {
            super(itemBinding.getRoot());

            this.binding = itemBinding;
        }

        void bind(Recipe recipe, RecipeUserActionListener actionsListener)
        {
            binding.setRecipe(recipe);
            binding.setListener(actionsListener);
        }

        void setupListener()
        {
            mListener = new RecipeUserActionListener()
            {
                @Override
                public void onItemClicked(Recipe recipe)
                {
                    mViewModel.setSelectedRecipe(recipe);
                }
            };
        }
    }

    private final LayoutInflater mInflater;
    private List<Recipe> mRecipes;
    private DashboardViewModel mViewModel;

    private RecipeUserActionListener mListener;

    public RecipeListAdapter(Context context, List<Recipe> recipes, DashboardViewModel viewModel)
    {
        this.mInflater = LayoutInflater.from(context);
        this.mRecipes = recipes;
        this.mViewModel = viewModel;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RecipeListViewBinding binding =
                DataBindingUtil.inflate(this.mInflater, R.layout.recipe_list_view, parent, false);

        return new RecipeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position)
    {
        holder.setupListener();

        Recipe recipeItem = this.mRecipes.get(holder.getAdapterPosition());

        holder.bind(recipeItem, mListener);
    }

    @Override
    public int getItemCount()
    {
        return this.mRecipes != null ? this.mRecipes.size() : 0;
    }
}
