package com.exam.cookingrecipe.dashboard.adapter;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.exam.cookingrecipe.utils.ImageConverter;

public class BindingAdapters
{
    @BindingAdapter("app:srcBitmap")
    public static void setSrcBitmap(ImageView imageView, byte[] bitmapByteArray)
    {
        if (null != bitmapByteArray)
        {
            imageView.setImageBitmap(ImageConverter.byteArrayToBitmap(bitmapByteArray));
        }
    }
}
