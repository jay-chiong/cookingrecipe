package com.exam.cookingrecipe.dashboard;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.dashboard.adapter.RecipeListAdapter;
import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.databinding.DashboardFragmentBinding;
import com.exam.cookingrecipe.filter.FilterViewModel;
import com.exam.cookingrecipe.recipeadd.RecipeAddFragment;
import com.exam.cookingrecipe.recipedetails.RecipeDetailsFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment
{
    private DashboardFragmentBinding mBinding;

    private RecipeListAdapter mAdapter;
    private List<Recipe> mRecipes = new ArrayList<>();

    private DashboardViewModel mDashboardViewModel;
    private FilterViewModel mFilterViewModel;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final View root = inflater.inflate(R.layout.dashboard_fragment, container, false);

        if (null == mBinding)
        {
            this.mBinding = DashboardFragmentBinding.bind(root);
        }

        mSharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();

        this.setupViewModel();
        this.setupAdapter();

        this.mBinding.setViewModel(this.mDashboardViewModel);

        return this.mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        this.setupFabClick();
    }

    private void setupAdapter()
    {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        this.mAdapter = new RecipeListAdapter(getActivity(), this.mRecipes, this.mDashboardViewModel);

        this.mBinding.recipeRecyclerView.setAdapter(this.mAdapter);
        this.mBinding.recipeRecyclerView.setLayoutManager(layoutManager);
    }

    private void setupViewModel()
    {
        this.mDashboardViewModel = this.obtainDashboardViewModel(getActivity());
        this.mFilterViewModel = this.obtainFilterViewModel(getActivity());

        this.getAllRecipes();

        this.mDashboardViewModel.mRecipes.observe(this, new Observer<List<Recipe>>()
        {
            @Override
            public void onChanged(List<Recipe> recipes)
            {
                if (recipes.size() > 0)
                {
                    mRecipes.clear();
                    mRecipes.addAll(recipes);
                    mAdapter.notifyDataSetChanged();
                }
                else
                {
                    if (mSharedPreferences.getBoolean("isFirstRun", true))
                    {
                        mDashboardViewModel.populateRecipes(getActivity());
                        editor.putBoolean("isFirstRun", false);
                        editor.commit();
                    }
                }
            }
        });

        this.mDashboardViewModel.selectedRecipe.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId)
            {
                if (null != mDashboardViewModel.selectedRecipe.get())
                {
                    switchToDetailsView();
                }
            }
        });

        this.mFilterViewModel.selectedFilterType.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId)
            {
                if (null != mFilterViewModel.selectedFilterType.get())
                {
                    if (mFilterViewModel.selectedFilterType.get().equalsIgnoreCase("All"))
                    {
                        getAllRecipes();
                    }
                    else
                    {
                        getRecipesWithFilter();
                    }
                }
            }
        });
    }

    private void getAllRecipes()
    {
        this.mDashboardViewModel.getRecipes().observe(this, new Observer<List<Recipe>>()
        {
            @Override
            public void onChanged(List<Recipe> recipes)
            {
                mDashboardViewModel.mRecipes.setValue(recipes);
            }
        });
    }

    private void getRecipesWithFilter()
    {
        this.mDashboardViewModel.getFilteredRecipes(mFilterViewModel.selectedFilterType.get())
                .observe(getActivity(), new Observer<List<Recipe>>()
                {
                    @Override
                    public void onChanged(List<Recipe> recipes)
                    {
                        mDashboardViewModel.mRecipes.setValue(recipes);
                    }
                });
    }

    private void setupFabClick()
    {
        FloatingActionButton fab = getActivity().findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                switchToAdd();
            }
        });
    }

    private DashboardViewModel obtainDashboardViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(DashboardViewModel.class);
    }

    private FilterViewModel obtainFilterViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(FilterViewModel.class);
    }

    private void switchToDetailsView()
    {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, new RecipeDetailsFragment());
        transaction.addToBackStack("details");
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }

    private void switchToAdd()
    {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, new RecipeAddFragment());
        transaction.addToBackStack("add");
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }
}
