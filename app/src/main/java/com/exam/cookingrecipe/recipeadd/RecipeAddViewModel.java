package com.exam.cookingrecipe.recipeadd;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.repository.RecipeRepository;

public class RecipeAddViewModel extends AndroidViewModel
{
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> type = new ObservableField<>();
    public final ObservableField<String> ingredients = new ObservableField<>();
    public final ObservableField<String> steps = new ObservableField<>();

    public final ObservableField<byte[]> imageByte = new ObservableField<>();

    private RecipeRepository mRepository;

    public RecipeAddViewModel(@NonNull Application application)
    {
        super(application);

        this.mRepository = new RecipeRepository(application);
    }

    public boolean validateInputs()
    {
        if (!TextUtils.isEmpty(name.get()) &&
            !TextUtils.isEmpty(type.get()) &&
            !TextUtils.isEmpty(ingredients.get()) &&
            !TextUtils.isEmpty(steps.get()) &&
            null != imageByte)
        {
            return true;
        }

        return false;
    }

    public void insert(Recipe recipe)
    {
        this.mRepository.insert(recipe);
    }

    public void notifyDataChange()
    {
        this.imageByte.notifyChange();
    }
}
