package com.exam.cookingrecipe.recipeadd;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.dashboard.DashboardActivity;
import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.databinding.RecipeAddLayoutBinding;
import com.exam.cookingrecipe.utils.ImageConverter;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class RecipeAddFragment extends Fragment
{
    private final int RESULT_LOAD_IMG = 1;

    private RecipeAddViewModel mRecipeAddViewModel;
    private RecipeAddLayoutBinding mBinding;

    private Spinner mRecipeTypeSpinner;
    private ArrayList<String> mRecipeTypes = new ArrayList<>();

    private byte[] mSelectedImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.recipe_add_layout, container, false);

        if (null == this.mBinding)
        {
            this.mBinding = RecipeAddLayoutBinding.bind(root);
        }

        this.setupViewModel();

        this.populateSpinner(root);
        this.setupButton(root);

        this.mBinding.setViewModel(this.mRecipeAddViewModel);

        return this.mBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.add_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        menu.findItem(R.id.action_filter).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_save:
                if (mRecipeAddViewModel.validateInputs())
                {
                    Recipe recipe = new Recipe(
                            mRecipeAddViewModel.name.get(),
                            mRecipeAddViewModel.type.get(),
                            mRecipeAddViewModel.ingredients.get(),
                            mRecipeAddViewModel.steps.get(),
                            mRecipeAddViewModel.imageByte.get()
                    );

                    mRecipeAddViewModel.insert(recipe);

                    getActivity().onBackPressed();
                }
                else
                {
                    Toast.makeText(getActivity(), "Please make sure everything is correct", Toast.LENGTH_SHORT).show();
                }

                break;
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK)
        {
            try
            {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                mSelectedImage = ImageConverter.bitmapToByteArray(bitmap);

                mRecipeAddViewModel.imageByte.set(mSelectedImage);
                mRecipeAddViewModel.notifyDataChange();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    private void populateSpinner(View root)
    {
        this.mRecipeTypeSpinner = root.findViewById(R.id.recipe_type_spinner);
        this.mRecipeTypes.addAll(Arrays.asList(getResources().getStringArray(R.array.recipe_types)));
        this.mRecipeTypes.remove(0);

        this.mRecipeAddViewModel.type.set(this.mRecipeTypes.get(0));

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                        this.mRecipeTypes);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.mRecipeTypeSpinner.setAdapter(adapter);

        this.mRecipeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
            {
                mRecipeAddViewModel.type.set(mRecipeTypes.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
    }

    private void setupButton(View root)
    {
        ImageButton changeImageButton = root.findViewById(R.id.add_image);

        changeImageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });
    }

    private void setupViewModel()
    {
        this.mRecipeAddViewModel = this.obtainRecipeAddViewModel(getActivity());
    }

    private RecipeAddViewModel obtainRecipeAddViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(RecipeAddViewModel.class);
    }
}
