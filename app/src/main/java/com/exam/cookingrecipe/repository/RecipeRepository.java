package com.exam.cookingrecipe.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.exam.cookingrecipe.RecipeRoomDatabase;
import com.exam.cookingrecipe.database.RecipeDao;
import com.exam.cookingrecipe.database.model.Recipe;

import java.util.ArrayList;
import java.util.List;

public class RecipeRepository
{
    private RecipeDao mRecipeDao;

    public RecipeRepository(Application application)
    {
        RecipeRoomDatabase db = RecipeRoomDatabase.getDatabase(application);
        this.mRecipeDao = db.recipeDao();
    }

    public LiveData<List<Recipe>> getAllRecipes()
    {
        return this.mRecipeDao.getAllRecipes();
    }

    public void insert(final Recipe recipe)
    {
        RecipeRoomDatabase.databaseWriteExecutor.execute(new Runnable()
        {
            @Override
            public void run()
            {
                mRecipeDao.insert(recipe);
            }
        });
    }

    public void insertAll(ArrayList<Recipe> recipes)
    {
        for (final Recipe recipe : recipes)
        {
            RecipeRoomDatabase.databaseWriteExecutor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    mRecipeDao.insert(recipe);
                }
            });
        }
    }

    public LiveData<List<Recipe>> getFilteredRecipes(final String recipeType)
    {
        return mRecipeDao.getRecipes(recipeType);
    }

    public void update(final Recipe recipe)
    {
        RecipeRoomDatabase.databaseWriteExecutor.execute(new Runnable()
        {
            @Override
            public void run()
            {
                mRecipeDao.updateRecipe(recipe);
            }
        });
    }
}
