package com.exam.cookingrecipe.recipedetails;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.exam.cookingrecipe.database.model.Recipe;
import com.exam.cookingrecipe.repository.RecipeRepository;

public class RecipeDetailsViewModel extends AndroidViewModel
{
    public final ObservableField<Recipe> recipe = new ObservableField<>();
    public final ObservableField<byte[]> imageByte = new ObservableField<>();

    public final ObservableBoolean editEnabled = new ObservableBoolean(false);

    private RecipeRepository mRepository;

    public RecipeDetailsViewModel(@NonNull Application application)
    {
        super(application);

        this.mRepository = new RecipeRepository(application);
    }

    public void updateRecipe(Recipe recipe)
    {
        this.mRepository.update(recipe);
    }

    public void notifyDataChange()
    {
        this.imageByte.notifyChange();
    }
}
