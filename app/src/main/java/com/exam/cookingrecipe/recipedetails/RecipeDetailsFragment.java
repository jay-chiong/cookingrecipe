package com.exam.cookingrecipe.recipedetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.dashboard.DashboardViewModel;
import com.exam.cookingrecipe.databinding.RecipeDetailsViewBinding;
import com.exam.cookingrecipe.utils.ImageConverter;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class RecipeDetailsFragment extends Fragment
{
    private final int RESULT_LOAD_IMG = 1;

    private RecipeDetailsViewBinding mBinding;
    private DashboardViewModel mDashboardViewModel;
    private RecipeDetailsViewModel mRecipeDetailsViewModel;

    private Menu mMenu;
    private byte[] selectedImage;
    private byte[] originalImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final View root = inflater.inflate(R.layout.recipe_details_view, container, false);

        if (null == mBinding)
        {
            this.mBinding = RecipeDetailsViewBinding.bind(root);
        }

        this.setupViewModel();
        this.setupButton(root);

        this.mBinding.setViewModel(mRecipeDetailsViewModel);
        this.mRecipeDetailsViewModel.recipe.set(this.mDashboardViewModel.selectedRecipe.get());
        this.mRecipeDetailsViewModel.imageByte.set(this.mRecipeDetailsViewModel.recipe.get().getImage());
        this.mBinding.setRecipe(this.mRecipeDetailsViewModel.recipe.get());

        this.originalImage = this.mDashboardViewModel.selectedRecipe.get().getImage();

        return this.mBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.edit_menu, menu);

        this.mMenu = menu;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_save).setVisible(false);
        menu.findItem(R.id.action_close).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_edit:
                mRecipeDetailsViewModel.editEnabled.set(true);
                mMenu.findItem(R.id.action_edit).setVisible(false);
                mMenu.findItem(R.id.action_save).setVisible(true);
                mMenu.findItem(R.id.action_close).setVisible(true);
                break;

            case R.id.action_close:
                hideSoftKeyboard();

                mRecipeDetailsViewModel.editEnabled.set(false);
                mMenu.findItem(R.id.action_save).setVisible(false);
                mMenu.findItem(R.id.action_close).setVisible(false);
                mMenu.findItem(R.id.action_edit).setVisible(true);

                mRecipeDetailsViewModel.recipe.get().setImage(this.originalImage);
                break;

            case R.id.action_save:
                hideSoftKeyboard();

                mRecipeDetailsViewModel.editEnabled.set(false);
                mMenu.findItem(R.id.action_save).setVisible(false);
                mMenu.findItem(R.id.action_close).setVisible(false);
                mMenu.findItem(R.id.action_edit).setVisible(true);

                mRecipeDetailsViewModel.recipe.get().setImage(this.selectedImage);
                mRecipeDetailsViewModel.updateRecipe(mRecipeDetailsViewModel.recipe.get());
                break;
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK)
        {
            try
            {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                selectedImage = ImageConverter.bitmapToByteArray(bitmap);
                mRecipeDetailsViewModel.recipe.get().setImage(selectedImage);

                mRecipeDetailsViewModel.imageByte.set(selectedImage);

                mRecipeDetailsViewModel.notifyDataChange();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
        else
            {
            Toast.makeText(getActivity(), "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    private void setupViewModel()
    {
        this.mDashboardViewModel = obtainDashboardViewModel(getActivity());
        this.mRecipeDetailsViewModel = obtainRecipeDetailsViewModel(getActivity());
    }

    private void setupButton(View root)
    {
        ImageButton changeImageButton = root.findViewById(R.id.change_image);

        changeImageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });
    }

    private DashboardViewModel obtainDashboardViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(DashboardViewModel.class);
    }

    private RecipeDetailsViewModel obtainRecipeDetailsViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(RecipeDetailsViewModel.class);
    }

    private void hideSoftKeyboard()
    {
        InputMethodManager imm =(InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mBinding.getRoot().getWindowToken(), 0);
    }
}
