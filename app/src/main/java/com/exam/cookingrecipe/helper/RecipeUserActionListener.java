package com.exam.cookingrecipe.helper;

import com.exam.cookingrecipe.database.model.Recipe;

public interface RecipeUserActionListener
{
    void onItemClicked(Recipe recipe);
}
