package com.exam.cookingrecipe.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.exam.cookingrecipe.database.model.Recipe;

import java.util.List;

@Dao
public interface RecipeDao
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Long insert(Recipe recipe);

    @Query("DELETE FROM recipe_table")
    void deleteAll();

    @Query("SELECT * from recipe_table")
    LiveData<List<Recipe>> getAllRecipes();

    @Query("SELECT * from recipe_table WHERE type = :recipeType")
    LiveData<List<Recipe>> getRecipes(String recipeType);

    @Update
    void updateRecipe(Recipe recipe);
}