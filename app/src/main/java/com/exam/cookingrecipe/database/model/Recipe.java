package com.exam.cookingrecipe.database.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "recipe_table")
public class Recipe
{
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "type")
    private String type;

    @NonNull
    @ColumnInfo(name = "ingredients")
    private String ingredients;

    @NonNull
    @ColumnInfo(name = "steps")
    private String steps;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB, name = "image")
    private byte[] image;

    public Recipe(String name, String type, String ingredients, String steps, byte[] image)
    {
        this.name = name;
        this.type = type;
        this.ingredients = ingredients;
        this.steps = steps;
        this.image = image;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setName(@NonNull String name)
    {
        this.name = name;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setType(@NonNull String type)
    {
        this.type = type;
    }

    @NonNull
    public String getType()
    {
        return type;
    }

    public void setIngredients(@NonNull String ingredients)
    {
        this.ingredients = ingredients;
    }

    @NonNull
    public String getIngredients()
    {
        return ingredients;
    }

    public void setSteps(@NonNull String steps)
    {
        this.steps = steps;
    }

    @NonNull
    public String getSteps()
    {
        return steps;
    }

    public byte[] getImage()
    {
        return image;
    }

    public void setImage(byte[] image)
    {
        this.image = image;
    }
}
