package com.exam.cookingrecipe.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class ImageConverter
{
    public static Bitmap byteArrayToBitmap(byte[] bitmapByteArray)
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapByteArray,
                0, bitmapByteArray.length);

        return bitmap;
    }

    public static byte[] bitmapToByteArray(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        return stream.toByteArray();
    }
}
