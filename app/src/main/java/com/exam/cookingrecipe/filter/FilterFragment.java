package com.exam.cookingrecipe.filter;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.exam.cookingrecipe.R;
import com.exam.cookingrecipe.databinding.FilterFragmentBinding;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class FilterFragment extends DialogFragment
{
    private FilterViewModel mFilterViewModel;
    private FilterFragmentBinding mBinding;

    private Spinner mRecipeTypeSpinner;
    private ArrayList<String> mRecipeTypes = new ArrayList<>();
    private String recipeType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View root = getLayoutInflater().inflate(R.layout.filter_fragment, container, false);

        if (null == mBinding)
        {
            this.mBinding = FilterFragmentBinding.bind(root);
        }

        this.setupViewModel();

        this.populateSpinner(root);
        this.initSaveButton(root);

        return this.mBinding.getRoot();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void setupViewModel()
    {
        this.mFilterViewModel = this.obtainViewModel(getActivity());
    }

    private void populateSpinner(View root)
    {
        this.mRecipeTypeSpinner = root.findViewById(R.id.filter_spinner);
        this.mRecipeTypes.addAll(Arrays.asList(getResources().getStringArray(R.array.recipe_types)));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.recipe_types, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.mRecipeTypeSpinner.setAdapter(adapter);

        this.mRecipeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
            {
                recipeType = mRecipeTypes.get(position);
                mFilterViewModel.selectedPosition.set(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });

        this.mRecipeTypeSpinner.setSelection(mFilterViewModel.selectedPosition.get());
    }

    private void initSaveButton(View root)
    {
        Button saveButton = root.findViewById(R.id.save);

        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // TODO: store sharedPref the recipe type
                mFilterViewModel.selectedFilterType.set(recipeType);
                getDialog().dismiss();
            }
        });
    }

    private FilterViewModel obtainViewModel(FragmentActivity activity)
    {
        return ViewModelProviders.of(activity).get(FilterViewModel.class);
    }
}
