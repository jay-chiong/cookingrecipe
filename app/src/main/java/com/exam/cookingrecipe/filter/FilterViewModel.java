package com.exam.cookingrecipe.filter;

import android.app.Application;
import android.database.Observable;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.AndroidViewModel;

public class FilterViewModel extends AndroidViewModel
{
    public final ObservableField<String> selectedFilterType = new ObservableField<>();
    public final ObservableInt selectedPosition = new ObservableInt(0);

    public FilterViewModel(@NonNull Application application) {
        super(application);
    }
}
